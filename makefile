fix:
	php-cs-fixer.phar fix -q src
	php-cs-fixer.phar fix -q tests
	
test:
	phpunit.phar
