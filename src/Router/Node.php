<?php

namespace Mparaiso\PhpRecipes\Router;

class Node
{
    /**
     * @var array
     */
    private $methods;
    /**
     * @var string
     */
    private $component;
    /**
     * @var array
     */
    private $children;
    /**
     * @var bool
     */
    private $isNamedParam;

    /**
     * Constructs a radix tree node.
     *
     * @param string $component
     * @param array  $methods
     * @param bool isNamedPAram
     */
    public function __construct(string $component = '', array $methods = [], bool $isNamedParam = false)
    {
        $this->component = $component;
        $this->methods = $methods;
        $this->isNamedParam = $isNamedParam;
        $this->children = [];
    }
    public function getMethods():array
    {
        return $this->methods;
    }
    public function getComponent():string
    {
        return $this->component;
    }

    public function addNode(array $components, string $method, callable $handler): ? Node
    {
        $count = count($components);
        if ($count === 1 && $components[0] == $this->component) {
            // update existing node
            $this->methods[$method] = $handler;

            return $this;
        }
        if ($count > 1 && $components[0] == $this->component) {
            $added = null;
            foreach ($this->children as $child) {
                $added = $child->addNode(array_slice($components, 1), $method, $handler);
                if ($added !== null) {
                    return $added;
                }
            }
            if ($added === null) {
                $component = $components[1];
                $isNamedParam = strpos($component, ':') === 0;
                $node = new self($component, [], $isNamedParam);
                array_push($this->children, $node);

                return $node->addNode(array_slice($components, 1), $method, $handler);
            }
        }

        return null;
    }

    public function searchNode(array $components, array &$urlParams) : ? Node
    {
        $len = count($components);

        if ($len > 0 && ($components[0] === $this->component || $this->isNamedParam)) {
            if ($this->isNamedParam === true) {
                $urlParams[\substr($this->component, 1)] = $components[0];
            }
            if ($len === 1) {
                return $this;
            }
            if (count($this->children) > 0) {
                foreach ($this->children as $child) {
                    $node = $child->searchNode(array_slice($components, 1), $urlParams);
                    if ($node !== null) {
                        return $node;
                    }
                }
            }
        }

        return null;
    }
}
