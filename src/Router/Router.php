<?php

namespace Mparaiso\PhpRecipes\Router;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Router
{
    /**
     * @var Node a tree of nodes
     */
    private $tree;

    /**
     * @var callable the default handler
     */
    private $defaultHandler;

    public function __construct(callable $defaultHandler)
    {
        $this->tree = new Node('', []);
        $this->defaultHandler = $defaultHandler;
    }
    /**
     * Associate a new handler to a path and method or replace an existing one.
     */
    public function HandleFunc(string $method, string $path, callable $handler)
    {
        if (strpos($path, '/') !== 0) {
            $path = join('/', $path);
        }
        $parts = (function () use ($path) {
            if ($path === "/") {
                return [''];
            }
            return explode("/", $path);
        })();
        $this->tree->addNode($parts, $method, $handler);
    }
    /**
     * handle a HTTPRequest and return a response.
     */
    public function serveHTTP(Response $response, Request $request)
    {
        $path = (function () use ($request) {
            if ($request->getPathInfo()=='/') {
                return [''];
            }
            return  explode($request->getPathInfo(), "/");
        })();
        $urlParams = [];
        $node = $this->tree->searchNode($path, $urlParams);
        if ($node!==null && array_key_exists($request->getMethod(), $node->getMethods())) {
            $handler = $node->getMethods()[$request->getMethod()];
            $handler($response, $request);
            return;
        }
        \call_user_func_array($this->defaultHandler, [$response,$request]);
    }
}
